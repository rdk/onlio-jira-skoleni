package onlio.jira;

import com.atlassian.sal.api.ApplicationProperties;

public class CustomManagerImpl implements CustomManager
{
    private final ApplicationProperties applicationProperties;

    public CustomManagerImpl(ApplicationProperties applicationProperties)
    {
        this.applicationProperties = applicationProperties;
    }

    public String getName()
    {
        if(null != applicationProperties)
        {
            return "myComponent:" + applicationProperties.getDisplayName();
        }
        
        return "myComponent";
    }
}