package onlio.jira.utils;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.status.Status;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DelayedTransition extends Thread {

    private static final Logger log = LoggerFactory.getLogger(DelayedTransition.class);

    int tryTimes = 3;
    int intervalMs = 4000;

    Issue issue;
    Status toStatus;
    User asUser;
    String comment;

    public DelayedTransition(Issue issue, Status toStatus, User asUser, String comment) {
        this.asUser = asUser;
        this.comment = comment;
        this.issue = issue;
        this.toStatus = toStatus;
    }

    public void setTryTimes(int tryTimes) {
        this.tryTimes = tryTimes;
    }

    public void setIntervalMs(int intervalMs) {
        this.intervalMs = intervalMs;
    }

    public void run(){
        try {
            ComponentAccessor.getJiraAuthenticationContext().setLoggedInUser(asUser);
            for (int i=0; i!=tryTimes; i++) {
                log.debug("delayed transition");

                boolean performed = Transition2Utils.performTransition(issue, toStatus, asUser, comment);
                if (performed) {
                    break;
                }

                sleep(intervalMs);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public static final void perform(Issue issue, Status toStatus, User asUser, String comment) {
        try {
            new DelayedTransition(issue, toStatus, asUser, comment).start();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

}
