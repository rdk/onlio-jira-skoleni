package onlio.jira.utils;


import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.status.Status;
import com.atlassian.jira.util.JiraUtils;
import com.atlassian.jira.workflow.IssueWorkflowManager;
import com.atlassian.jira.workflow.JiraWorkflow;
import com.atlassian.jira.workflow.WorkflowTransitionUtil;
import com.atlassian.jira.workflow.WorkflowTransitionUtilImpl;
import com.opensymphony.workflow.loader.ActionDescriptor;
import com.opensymphony.workflow.loader.StepDescriptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

/**
 * @see com.atlassian.jira.jelly.tag.issue.TransitionWorkflow
 */
public class Transition2Utils {

    private static final Logger log = LoggerFactory.getLogger(Transition2Utils.class);

    /**
     *
     * @param issue
     * @param toStatus
     * @param comment
     */
    public static boolean performTransition(Issue issue, Status toStatus, String comment) {
        User user = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser();
        return performTransition(issue, toStatus, user, comment);
    }


    public static boolean performTransition(Issue issue, Status toStatus, User asUser, String comment) {
        return performTransition(issue, toStatus, asUser, comment, false);
    }

    public static void performTransitionDelayed(Issue issue, Status toStatus, String comment) {
        User user = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser();
        performTransitionDelayed(issue, toStatus, user, comment);
    }

    public static void performTransitionDelayed(Issue issue, Status toStatus, User asUser, String comment) {
        DelayedTransition.perform(issue, toStatus, asUser, comment);
    }




    /**
     *
     * @param issue
     * @param toStatus
     * @param asUser
     * @param testingMode will perform validation but not transition itself
     * @param comment  null -> no comment added
     */
    public static boolean performTransition(Issue issue, Status toStatus, User asUser, final String comment, boolean testingMode) {

        try {
            MutableIssue mutableIssue = ComponentAccessor.getIssueManager().getIssueObject(issue.getId());

            ActionDescriptor action = getAvailableAction(issue, toStatus, asUser);

            if (action==null) {

                log.warn("workflow transition of issue {} from status '"
                        +issue.getStatusObject().getName()
                        +"' to status '{}' does not exist or user '"
                        +UserGroupUtils.getUserName(asUser)
                        +"' has insufficient permissions", issue.getKey(), toStatus.getName());

            } else {
                WorkflowTransitionUtil wtu = (WorkflowTransitionUtil) JiraUtils.loadComponent(WorkflowTransitionUtilImpl.class);
                wtu.setIssue(mutableIssue);
                wtu.setUsername(asUser.getName());
                wtu.setAction(action.getId());

                if (comment!=null) {
                    wtu.setParams(new HashMap() {{
                        put(WorkflowTransitionUtil.FIELD_COMMENT, comment);
                    }});
                }

                String msg = "performing transition '"+action.getName()+"' on issue "+issue.getKey();
                log.info(msg+" [{}->{}]", issue.getStatusObject().getName(), action);

                // validate and transition issue
                wtu.validate();
                if (true || !testingMode) {
                    wtu.progress();

                    //reindex issue here? not needed
                    //IssueUtils.reindexIssue(issue);

                    return true;
                } else {
                    log.info("testing mode: skipping the actual transition");
                }
            }
        } catch (Exception e) {
            log.error("error performing transition on issue "+issue.getKey()+" to state "+toStatus.getName(), e);
        }

        return false;
    }



    ///workflow/descriptor/step(transition/unconditionalResult/step)

    public static ActionDescriptor getAvailableAction(Issue issue, Status status, User forUser) {

        // available actions depends on currently logged in user!
        User origUser = UserGroupUtils.getCurrentUser();
        UserGroupUtils.setCurrentUser(forUser);

        ActionDescriptor res = null;

        try {
            IssueWorkflowManager ivm = ComponentAccessor.getComponent(IssueWorkflowManager.class);
            Collection<ActionDescriptor> actions = ivm.getAvailableActions(issue);

            JiraWorkflow w = ComponentAccessor.getWorkflowManager().getWorkflow(issue);

            for (ActionDescriptor a : actions) {
                StepDescriptor step = getStepWithTargetStatus(a, w, status);
                if (step!=null) {
                    res = a;
                    break;
                }
            }
        } finally {
            UserGroupUtils.setCurrentUser(origUser);
        }

        return res;
    }

    public static StepDescriptor getStepWithTargetStatus(ActionDescriptor a, JiraWorkflow w, Status status) {
        log.debug("available action: {}", a.getName());

        //        log.info(ToStringBuilder.reflectionToString(a, ToStringStyle.MULTI_LINE_STYLE)+"\n");
        //        log.info(ToStringBuilder.reflectionToString(a.getUnconditionalResult(), ToStringStyle.MULTI_LINE_STYLE)+"\n");
        //        log.info(ToStringBuilder.reflectionToString(a.getConditionalResults(), ToStringStyle.MULTI_LINE_STYLE)+"\n");
        //        log.info(ToStringBuilder.reflectionToString(a.getRestriction(), ToStringStyle.MULTI_LINE_STYLE)+"\n\n\n");


        List<StepDescriptor> candidateSteps = new LinkedList<StepDescriptor>();
        StepDescriptor resultStep = w.getDescriptor().getStep(a.getUnconditionalResult().getStep());
        if (resultStep!=null) {
            candidateSteps.add(resultStep);
        }

        // causes incorrect transition for timeout rules!
        //        Collection<StepDescriptor> otherSteps = w.getStepsForTransition(a);
        //        if (otherSteps!=null) {
        //            candidateSteps.addAll(otherSteps);
        //        }


        for (StepDescriptor step : candidateSteps) {
            // log.info("STEP: " + ToStringBuilder.reflectionToString(s, ToStringStyle.MULTI_LINE_STYLE)+"\n\n\n");
            Status linkedStatus = getLinkedStatusObject(step);

//            log.debug("step "+step.getName());
//            log.debug("linkedStatus id:{} name:{}", status.getId(), status.getName());
//            log.debug("      status id:{} name:{}", linkedStatus.getId(), linkedStatus.getName());

            if (linkedStatus!=null && status.getId().equals(linkedStatus.getId())) {
                return step;
            }
        }

        return null;

    }


    public static Status getLinkedStatusObject(StepDescriptor step) {
        // see viewworkflowstep.jsp:28 in jira
        String linkedStatusId = (String) step.getMetaAttributes().get("jira.status.id");
        return ComponentAccessor.getConstantsManager().getStatusObject(linkedStatusId);
    }


}
