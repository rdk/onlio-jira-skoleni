package onlio.jira.utils;

import com.atlassian.jira.ComponentManager;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.util.JiraHome;
import com.atlassian.jira.propertyset.JiraPropertySetFactory;
import com.opensymphony.module.propertyset.PropertySet;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.util.*;

public class PropertiesUtils {

    private static final Logger log = LoggerFactory.getLogger(PropertiesUtils.class);


    private static final String STRING_LIST_SEPARATOR = "\n";
	
	private static final String MAP_KEY_VALUE_SEPARATOR = ":=";
	
//================================================================================================//	
	
	public static List<String> loadStringList(String propertyKey, String entityName, Long entityId) {
		PropertySet propertySet = getPropertySetForEntity(entityName, entityId);
		return loadStringList(propertyKey, propertySet);
	}
	
	public static void storeStringList(List<String> list, String propertyKey, String entityName, Long entityId) {
		PropertySet propertySet = getPropertySetForEntity(entityName, entityId);
		storeStringList(list, propertyKey, propertySet);
	}
	
	
	public static List<String> loadStringList(String propertyKey, PropertySet propertySet) {
		String text = propertySet.getText(propertyKey);
		if (StringUtils.isBlank(text)) {
			return Collections.emptyList();
		}
		List<String> strings = Arrays.asList(StringUtils.split(text, STRING_LIST_SEPARATOR));
		return strings;
	}

	public static void storeStringList(List<String> list, String propertyKey, PropertySet propertySet) {
		String value = StringUtils.join(list.iterator(), STRING_LIST_SEPARATOR);
		propertySet.setText(propertyKey, value);
	}
	
// ================================================================================================//	
	
	/**
	 * 
	 * @param propertyKey
	 * @param propertySet
	 * @return
	 */
	public static Map<String, String> loadStringMap(String propertyKey, PropertySet propertySet) {
		return parseStringMap(loadStringList(propertyKey, propertySet));
	}

	/**
	 * ! ":=" and newline are used as separators
	 * @param stringMap
	 * @param propertyKey
	 * @param propertySet
	 */
	public static void storeStringMap(Map<String, String> stringMap, String propertyKey, PropertySet propertySet) {
		storeStringList(formatStringMap(stringMap), propertyKey, propertySet);
	}
	
	
// ================================================================================================//	
		
	public static Map<String, String> parseStringMap(List<String> strings) {
		Map<String, String> assigneeMap = new HashMap<String, String>();
		
		for (String s : strings) {
			if (!StringUtils.isBlank(s)) {
				 String[] ss = StringUtils.splitByWholeSeparator(s, MAP_KEY_VALUE_SEPARATOR);
				 if (ss.length==2) {
					
					String key = ss[0];
					String value = ss[1];
					
					assigneeMap.put(key, value);
				 }
			}
		}
		
		return assigneeMap;
	}
	
	/**
	 * null keys and blank values are ignored!
	 */
	public static List<String> formatStringMap(Map<String, String> assigneeMap) {
		List<String> strings = new ArrayList<String>(assigneeMap.size());
		
		for (Map.Entry<String, String> entry : assigneeMap.entrySet()) {
			
			String key = entry.getKey();
			String value = entry.getValue();
			
			if (key!=null && !StringUtils.isBlank(value)) {
				key = key.replace(MAP_KEY_VALUE_SEPARATOR, "");
				value = value.replace(MAP_KEY_VALUE_SEPARATOR, "");
				
				String s = key + MAP_KEY_VALUE_SEPARATOR + value;
				strings.add(s);
			}

		}
		
		return strings;
	}	
	
	
//================================================================================================//	
	
	public static JiraPropertySetFactory getPropertySetFactory() {
		return (JiraPropertySetFactory) ComponentManager.getComponentInstanceOfType(JiraPropertySetFactory.class);
	}
	
	public static PropertySet getPropertySetForEntity(String entityName, Long entityId) {
		PropertySet propertySet = getPropertySetFactory().buildCachingPropertySet(entityName, entityId, false);
		return propertySet;
	}


//===============================================================================================//

    /**
     * <jira.home>/file.properties
     * @param fileName eg. file.properties
     * @return never null
     */
    public static Properties loadPropertyFileFromJiraHome(String fileName) {
        Properties p = new Properties();
        try {
            File jiraHome = ComponentAccessor.getComponent(JiraHome.class).getHome();
            File propertyFile = new File(jiraHome, fileName);
            if(propertyFile.exists()) {
                log.debug("loading property file '{}'", propertyFile.getAbsolutePath());
                FileInputStream fileInputStream = new FileInputStream(propertyFile);
                p.load(fileInputStream);
            } else {
                log.debug("property file doesnt exist '{}' ", propertyFile.getAbsolutePath());
            }
        } catch (Exception e) {
            log.error("error loading properties '{}'", fileName);
        }
        return p;
    }

    /**
     * <jira.home>/file.properties
     * @param fileName eg. file.properties
     * @return never null
     */
    public static Configuration loadPropertyConfigurationFromJiraHome(String fileName) {
        try {
            File jiraHome = ComponentAccessor.getComponent(JiraHome.class).getHome();
            File propertyFile = new File(jiraHome, fileName);
            if(propertyFile.exists()) {
                log.debug("loading property file '{}'", propertyFile.getAbsolutePath());

                return new PropertiesConfiguration(propertyFile);
            } else {
                log.debug("property file doesnt exist '{}' ", propertyFile.getAbsolutePath());
            }
        } catch (Exception e) {
            log.error("error loading properties '{}'", fileName);
        }
        return new PropertiesConfiguration();
    }

}
