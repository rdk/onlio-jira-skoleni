package onlio.jira.utils;

import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.user.util.UserManager;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;


public class UserGroupUtils {

	public static boolean existsUser(String userName) {
		return ComponentAccessor.getUserUtil().userExists(userName);
	}

//================================================================================================//

	public static User getCurrentUser() {
		return ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser();
	}

    public static void setCurrentUser(User user) {
        ComponentAccessor.getJiraAuthenticationContext().setLoggedInUser(user);
    }

	public static boolean isCurrentUserInGroup(String groupName) {
		return isUserInGroup(getCurrentUser(), groupName);
	}
	
	public static boolean isUserInGroup(User user, String groupName) {
		if (user==null) return false;
		
		return ComponentAccessor.getGroupManager().isUserInGroup(user.getName(), groupName);
	}
	
	public static boolean isUserInGroup(String userName, String groupName) {
		if (userName==null) return false;
		
		return ComponentAccessor.getGroupManager().isUserInGroup(userName, groupName);
	}

    public static String getUserName(User user) {
        if (user==null) return null;
        return user.getName();
    }


// ================================================================================================//	
	
	public static Collection<User> getMembersOfGroup(String groupName) {
		if (groupName==null) return Collections.emptyList();
			
	    return ComponentAccessor.getGroupManager().getUsersInGroup(groupName);
	}

	public static Collection<User> getMembersOfGroup(Group group) {
		if (group==null) return Collections.emptyList();
			
	    return ComponentAccessor.getGroupManager().getUsersInGroup(group);
	}

	public static Collection<String> getUserNamesInGroup(String groupName) {
		if (groupName==null) return Collections.emptyList();
			
	    return ComponentAccessor.getGroupManager().getUserNamesInGroup(groupName);
	}

	public static Collection<String> getUserNamesInGroup(Group group) {
		if (group==null) return Collections.emptyList();
	    	
		return ComponentAccessor.getGroupManager().getUserNamesInGroup(group);
	}
	
// ================================================================================================//	


	public static List<User> convertToUsers(Collection<String> userNames) {
		if (userNames==null) return Collections.emptyList();
		List<User> users = new ArrayList<User>(userNames.size());

		UserManager userManager = ComponentAccessor.getUserManager();

		for (String name : userNames) {
			User user = userManager.getUser(name);
			if (user != null) {
				users.add(user);
			} else {
				log.warn("Cannot find user with name [" + name + "]");
			}
		}
		return users;
	}

	public static Group getGroup(String groupName) {
		return ComponentAccessor.getGroupManager().getGroup(groupName);
	}

	public static User getUser(String userName) {
		return ComponentAccessor.getUserManager().getUser(userName);
	}


//================================================================================================//

	private static final Logger log = Logger.getLogger(UserGroupUtils.class);



}
