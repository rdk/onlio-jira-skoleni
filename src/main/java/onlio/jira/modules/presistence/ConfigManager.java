package onlio.jira.modules.presistence;

import onlio.jira.utils.PropertiesUtils;
import org.apache.commons.configuration.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConfigManager {

    private static final Logger log = LoggerFactory.getLogger(ConfigManager.class);

    // properties
    private String executeAsUser = "admin";
    private int jobRepeatInterval = 60;
    private boolean testingMode = false;

    public ConfigManager() {

        loadProperties();
    }

// ================================================================================================//

    private void loadProperties() {
        try {
            Configuration conf = PropertiesUtils.loadPropertyConfigurationFromJiraHome("onlio-workflow-extensions.properties");

            executeAsUser = conf.getString("execute.as.user", executeAsUser);
            jobRepeatInterval = conf.getInt("job.repeat.interval", jobRepeatInterval);
            testingMode = conf.getBoolean("testing.mode", testingMode);
        } catch (Exception e) {
            log.error("error loading properties", e);
        }
    }



}
