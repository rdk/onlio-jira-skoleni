package onlio.jira.modules.listener;

import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.event.issue.IssueEvent;
import com.atlassian.jira.event.type.EventType;
import com.google.common.collect.ImmutableSet;

/**
 * new type listener: not used as traditional jira listener but rather using atklassian event system
 *
 * must be registered as component in atlassian-plugin.xml
 */
public class CustomIssueListener implements InitializingBean, DisposableBean  {

    private static final Logger log = LoggerFactory.getLogger(CustomIssueListener.class);

    private final EventPublisher eventPublisher;
    
    /**
     * Constructor.
     * @param eventPublisher injected {@code EventPublisher} implementation.
     */
    public CustomIssueListener(EventPublisher eventPublisher) {
        this.eventPublisher = eventPublisher;
    }	
	
// ================================================================================================//	
	
	@Override
	public void destroy() throws Exception {
		log.debug("Unregistering CustomIssueListener");
	    eventPublisher.unregister(this);
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		log.debug("Registering CustomIssueListener");
        eventPublisher.register(this);
	}	
	
// ================================================================================================//	
	
	private static Set<Long> IGNORED_EVENT_TYPES = ImmutableSet.of(
			EventType.ISSUE_CREATED_ID,    // issue create event is handled by PostCreateListener
			EventType.ISSUE_ASSIGNED_ID,
			EventType.ISSUE_COMMENTED_ID,
			EventType.ISSUE_COMMENTED_ID,
			EventType.ISSUE_DELETED_ID,
			EventType.ISSUE_WORKLOG_DELETED_ID,
			EventType.ISSUE_WORKLOG_UPDATED_ID,
			EventType.ISSUE_WORKLOGGED_ID,
			EventType.ISSUE_WORKSTARTED_ID,
			EventType.ISSUE_WORKSTOPPED_ID
			);
	
	
    /**
     * Receives any {@code IssueEvent}s sent by JIRA.
     * @param event the IssueEvent passed to us
     */
    @EventListener
    public void onIssueEvent(IssueEvent event) {
 
        //if (!IGNORED_EVENT_TYPES.contains(event.getEventTypeId())) { // we want to catch events also on workflow transitions
            issueUpdated(event);
        //}

    }	
	
	
	public void issueUpdated(IssueEvent event) {
		
		try {

            log.info("EVENT type:{} issue:{}", event.getEventTypeId(), event.getIssue().getKey());

		} catch (Exception e) {
			String issueKey = (event.getIssue()!=null) ? event.getIssue().getKey() : null;
			log.error("Error while processing event for an issue "+issueKey, e);
		}
		
	}
	
	

}