package onlio.jira.modules.gui;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.ApplicationUsers;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import onlio.jira.CustomManager;
import onlio.jira.utils.UserGroupUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sun.awt.AWTAccessor;

import java.net.URLEncoder;

public class CustomConfigAction extends JiraWebActionSupport {
	
	
	private static final String SECURITYBREACH_RESULT = "securitybreach";
	
	private final CustomManager customManager;
	
// ================================================================================================//
	
	private String employeeName;
	private String supervisorName;

	private String message;
	

// ================================================================================================//
	
	public CustomConfigAction(CustomManager customManager) {
		this.customManager = customManager;
	}
	
	
	private void init() {

	}	
	


// ================================================================================================//
	
	private String redirect() {
		String url = "/secure/CustomConfig.jspa";
		if (message!=null) {
			url+="?message="+ URLEncoder.encode(message);
		}
		
		return getRedirect(url);
	}
	
// ================================================================================================//	
	
	/**
	 * view
	 */
	public String doExecute() {
		if (UserGroupUtils.isCurrentUserInGroup("jira-administrators")) return SECURITYBREACH_RESULT;
		
		init();

		return SUCCESS;
	}



// ================================================================================================//	
	
	private static final Logger log = LoggerFactory.getLogger(CustomConfigAction.class);

}
