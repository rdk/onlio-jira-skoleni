package onlio.jira.modules.job;

import com.atlassian.core.util.map.EasyMap;
import com.atlassian.sal.api.lifecycle.LifecycleAware;
import com.atlassian.sal.api.scheduling.PluginScheduler;
import onlio.jira.CustomManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

import java.util.Date;
import java.util.Map;

/**
 * podla https://developer.atlassian.com/display/DOCS/Plugin%20Tutorial%20-%20Scheduling%20Events%20via%20SAL
 *
 */
public class CustomJobScheduler implements LifecycleAware, InitializingBean, DisposableBean  {

	private static final int REPEAT_MINS = 1;
//	private static final String JOB_KEY = FarAwayUpdateJob.class.getName(); // com.onlio.jira.plugins.faraway.modules.service.FarAwayUpdateJob
	private static final String JOB_KEY = "Onlio Custom Job";
	private static final Logger log = LoggerFactory.getLogger(CustomJobScheduler.class);
	
// ================================================================================================//	
	
	private PluginScheduler pluginScheduler;
	private CustomManager customManager;


// ================================================================================================//

	public CustomJobScheduler(PluginScheduler pluginScheduler, CustomManager customManager) {
		this.pluginScheduler = pluginScheduler;
        this.customManager = customManager;
	}


	/**
	 * called on jira start after jira is properly initialized
	 * 
	 * see
	 * https://developer.atlassian.com/display/JIRADEV/JIRA+Plugin+Lifecycle
	 */
	@Override
	public void onStart() {
		log.debug("onStart() called");
		reschedule();
	}
	
	
	@Override
	public void afterPropertiesSet() throws Exception {
		log.debug("afterPropertiesSet() called");
		reschedule();
	}
	
	@Override
	public void destroy() throws Exception {
		try {
			log.debug("Unscheduling Job");
			pluginScheduler.unscheduleJob(JOB_KEY);
		} catch (Exception e) {
			log.warn("Error while unscheduling Job");
		}
	}
	
	@SuppressWarnings("unchecked")
	public void reschedule() {
		
//		try {
//			pluginScheduler.unscheduleJob(JOB_KEY);
//		} catch (Exception e) {
//			// nothing
//		}
		
		try {
			
			long repeatInterval = REPEAT_MINS * 60 * 1000;
			pluginScheduler.scheduleJob(
					JOB_KEY, 
					CustomJob.class,
                    (Map)EasyMap.build("customManager", customManager),
					new Date(),
					repeatInterval);
			
			log.debug("Job Scheduled");
		} catch (Exception e) {
			log.warn("Problem when scheduling Custom Job (ok if jira is starting)");
		}
		
		

	}
	
}
