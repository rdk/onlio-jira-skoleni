package onlio.jira.modules.job;

import com.atlassian.sal.api.scheduling.PluginJob;
import onlio.jira.CustomManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * changed from jira service to quartz job because of problems with services and plugin v2 osgi components 
 *
 */
public class CustomJob implements PluginJob {

	public CustomJob() {
		log.info("Custom Job initialized");
	}
	
	@Override
	public void execute(Map<String, Object> jobDataMap) {
		log.debug("Custom Job Started");

		try {

			CustomManager customManager = (CustomManager) jobDataMap.get("customManager");

            log.info("Custom Job EXECUTED!");

            // customManager.getName(); ... something
			

			log.debug("Custom Job Finished Without Errors");
		} catch (Exception e) {
			log.error("Error while updating vacation groups", e);
		}
	}



//================================================================================================//
	
	private static final Logger log = LoggerFactory.getLogger(CustomJob.class);




}
