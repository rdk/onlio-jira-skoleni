package onlio.jira;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.bc.project.component.ProjectComponent;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.security.IssueSecurityLevel;
import com.atlassian.jira.issue.security.IssueSecurityLevelManager;
import com.atlassian.jira.issue.security.IssueSecuritySchemeManager;
import com.atlassian.jira.notification.NotificationRecipient;
import com.atlassian.jira.plugin.workflow.WorkflowPluginFunctionFactory;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.scheme.Scheme;
import com.atlassian.jira.workflow.function.issue.AbstractJiraFunctionProvider;
import com.google.common.collect.ImmutableSet;
import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.WorkflowException;
import com.opensymphony.workflow.loader.AbstractDescriptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sun.awt.AWTAccessor;

import java.util.Collections;
import java.util.List;
import java.util.Map;


public class SecLevelPostFunction extends AbstractJiraFunctionProvider implements WorkflowPluginFunctionFactory {

    public static final String FAILBACK_SECURITY_Level_NAME = "99999";


    private static final Logger log = LoggerFactory.getLogger(SecLevelPostFunction.class);

    @Override
    public void execute(Map transientVars, Map args, PropertySet ps) throws WorkflowException {

        MutableIssue issue = getIssue(transientVars);

        try {
            IssueSecurityLevel newLevel =  selectSecurityLevelForIssue(issue);

            if (newLevel!=null) {
                log.debug("Setting security level of issue [{}] to [{}]", issue.getKey(), newLevel.getName());
                issue.setSecurityLevelId(newLevel.getId());

                if (issue.isCreated()) {
                    issue.store();
                }

            } else {
                log.debug("No security level is mapped for issue [{}]", issue.getKey());
            }

        } catch (Exception e) {
            log.error("Error while assigning issue to security type"+issue.getKey(), e);
        }

    }


    private static IssueSecurityLevelManager getSecurityLevelManager() {
        return ComponentAccessor.getIssueSecurityLevelManager();
    }

    private static IssueSecuritySchemeManager getSecuritySchemeManager() {
        return ComponentAccessor.getComponent(IssueSecuritySchemeManager.class);
    }

    private static List<IssueSecurityLevel> getSecurityLevelsForProject(Project project) {
        Scheme scheme = getSecuritySchemeManager().getSchemeFor(project);

        List<IssueSecurityLevel> levels = Collections.emptyList();
        if (scheme!=null) {
            levels = getSecurityLevelManager().getIssueSecurityLevels(scheme.getId());
        }

        return levels;
    }

    private IssueSecurityLevel selectSecurityLevelForIssue(Issue issue) {

        List<IssueSecurityLevel> levels = getSecurityLevelsForProject(issue.getProjectObject());

        // try select level by customfield
        for (ProjectComponent comp : issue.getComponentObjects()) {
            for (IssueSecurityLevel level : levels) {
                if (matchName(comp.getName(), level.getName())) {
                    return level;
                }
            }
        }

        // failback level
        for (IssueSecurityLevel level : levels) {
            if (FAILBACK_SECURITY_Level_NAME.equals(level.getName())) {
                return level;
            }
        }

        return null;
    }

    private boolean matchName(String cfValue, String levelName) {
        if (cfValue==null || levelName==null) return false;
        return (levelName.trim().equalsIgnoreCase(cfValue.trim()));
    }

// ================================================================================================//

    // WorkflowPluginFunctionFactory methods

    @Override
    public Map<String, ?> getVelocityParams(String paramString, AbstractDescriptor paramAbstractDescriptor) {
        return Collections.emptyMap();
    }

    @Override
    public Map<String, ?> getDescriptorParams(Map<String, Object> paramMap) {
        // TODO Auto-generated method stub
        return Collections.emptyMap();
    }

}
