package ut.onlio.jira;

import org.junit.Test;
import onlio.jira.CustomManager;
import onlio.jira.CustomManagerImpl;

import static org.junit.Assert.assertEquals;

public class MyComponentUnitTest
{
    @Test
    public void testMyName()
    {
        CustomManager component = new CustomManagerImpl(null);
        assertEquals("names do not match!", "myComponent",component.getName());
    }
}